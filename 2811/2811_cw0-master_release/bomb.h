#ifndef BOMB_H
#define BOMB_H


#include "thing.h"

// Bomb can destory coins and mushrooms
class Bomb : public Thing
{
    string getName()
    {
        return "bomb";
    }
};
#endif // BOMB_H
