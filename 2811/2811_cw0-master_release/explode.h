#ifndef EXPLODE_H
#define EXPLODE_H

#include "cave.h"
#include "command.h"
#include <string>
#include <iostream>
#include <string>

using namespace std;

class Explode : public Command
{
public:
    Explode() : Command("throw") {};
    void fire(Cave& c, string userCommand);
};

#endif // EXPLODE_H
