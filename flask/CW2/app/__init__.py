from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask import session
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms.validators import DataRequired
import logging


app = Flask(__name__)
app.config.from_object('config')
db = SQLAlchemy(app)
db.init_app(app)
app.secret_key = 'abc'
app.config['SESSION_PERMANENT'] = False
app.config['SESSION_TYPE'] = 'filesystem'
logging.basicConfig(level=logging.DEBUG)

migrate = Migrate(app, db)



from app import views,module

