from flask import render_template
from flask import Flask, session
from flask import request
from flask import redirect
from app import app
from app import db
from app.module import User
from app.module import NS1
from flask_session import Session


@app.route("/", methods =['GET'])
def index():
    return render_template("index.html")

@app.route("/login", methods =['POST','GET'])
def login():
     username=request.form['username']
     password = request.form['password']
     isPasswordCorrect = userinformation(username, password)
     app.logger.info("isPasswordCorrect is %s",isPasswordCorrect )
     if isPasswordCorrect == True:         
          return redirect("/booklist")
     else: 
          error = "password or account is not correct"
          return error

def userinformation(username,password):
     app.logger.info("username %s ",username )
     user = User.query.filter_by(username = username).first()
     if user.username == username and user.password == password:
          return True
     else:                              
          return False
          


@app.route("/register",methods=['POST','GET'])
def register():
     if request.method == 'POST':
          username = request.form['username']
          password = request.form['password']
          Users=User(username=username,password=password)
          db.session.add(Users)
          db.session.commit()
     return render_template("register.html")


@app.route("/booklist",methods=['POST','GET'])
def booklist():

     return render_template("booklist.html", NSs=NS1.query.all())
@app.route("/logout",methods=['POST','GET'])
def logout():
     return render_template('index.html')

@app.route("/Comp2711",methods=['POST','GET'])
def Comp2711():
     Booksname = {'name': 'Introduction to the design & analysis of algorithms'}
     Bookauthor = {'name': 'Levitin. (2012). Introduction to the design & analysis of algorithms  (Third edition.). Pearson.'}
     return render_template('Comp2711.html',
                           title='Simple template example',
                           Booksname=Booksname,Bookauthor=Bookauthor)
@app.route("/add-favor", methods=["POST"])   
def NS():    
     if request.method == 'POST':
          Booksname = request.form['Booksname']
          Bookauthor = request.form['Bookauthor']
          NSs=NS1(Booksname=Booksname,Bookauthor=Bookauthor)
          db.session.add(NSs)
          db.session.commit()
          return redirect('/booklist')
     return render_template("Comp2711.html")

     

@app.route("/Comp2421")
def Comp2421():
     Booksname = {'name': 'Engineering mathematics'}
     Bookauthor = {'name': 'Stroud. (2020). Engineering mathematics  (Booth, Ed.; Eighth edition.). Red Globe Press.'}
     return render_template('Comp2421.html',
                           title='Simple template example',
                           Booksname=Booksname,Bookauthor=Bookauthor)
     

@app.route("/Comp2811")
def Comp2811():
     Booksname = {'name': 'Interaction Design: Beyond Human-Computer Interaction'}
     Bookauthor = {'name': 'Sharp. (2019). Interaction Design: Beyond Human-Computer Interaction (the fifth time Edition). Wiley.'}
     return render_template('Comp2811.html',
                           title='Simple template example',
                           Booksname=Booksname,Bookauthor=Bookauthor)
     
@app.route("/Comp2912")
def Comp2912():
     Booksname = {'name': 'Software Engineering'}
     Bookauthor = {'name': 'Sommerville. (2016). Software engineering  (Tenth edition, Global edition.). Pearson.'}
     return render_template('Comp2912.html',
                           title='Simple template example',
                           Booksname=Booksname,Bookauthor=Bookauthor)

@app.route("/delete-favor", methods=["POST"])
def deletefavor():
     id = request.form["id"]
     ns = NS1.query.get(id)
     db.session.delete(ns)
     db.session.commit()    
     return redirect("/booklist")
