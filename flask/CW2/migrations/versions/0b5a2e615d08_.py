"""empty message

Revision ID: 0b5a2e615d08
Revises: 12a53f99e306
Create Date: 2022-12-26 15:52:02.098306

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '0b5a2e615d08'
down_revision = '12a53f99e306'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index('ix_comp2711_UI', table_name='comp2711')
    op.drop_table('comp2711')
    op.drop_index('ix_NS_Booksname', table_name='NS')
    op.drop_table('NS')
    op.drop_index('ix_collect_username', table_name='collect')
    op.drop_table('collect')
    op.alter_column('user', 'username',
               existing_type=sa.VARCHAR(),
               nullable=False)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('user', 'username',
               existing_type=sa.VARCHAR(),
               nullable=True)
    op.create_table('collect',
    sa.Column('id', sa.INTEGER(), nullable=False),
    sa.Column('username', sa.VARCHAR(length=100), nullable=False),
    sa.Column('password', sa.VARCHAR(length=220), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index('ix_collect_username', 'collect', ['username'], unique=False)
    op.create_table('NS',
    sa.Column('id', sa.INTEGER(), nullable=False),
    sa.Column('Booksname', sa.VARCHAR(length=100), nullable=False),
    sa.Column('Bookauthor', sa.VARCHAR(length=220), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index('ix_NS_Booksname', 'NS', ['Booksname'], unique=False)
    op.create_table('comp2711',
    sa.Column('id', sa.INTEGER(), nullable=False),
    sa.Column('UI', sa.VARCHAR(length=100), nullable=False),
    sa.Column('UI2', sa.VARCHAR(length=220), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index('ix_comp2711_UI', 'comp2711', ['UI'], unique=False)
    # ### end Alembic commands ###
